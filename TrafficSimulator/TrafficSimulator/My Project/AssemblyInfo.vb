﻿Imports System
Imports System.Reflection
Imports System.Runtime.InteropServices

' General Information about an assembly is controlled through the following 
' set of attributes. Change these attribute values to modify the information
' associated with an assembly.

' Review the values of the assembly attributes

<Assembly: AssemblyTitle("TrafficSimulator")> 
<Assembly: AssemblyDescription("A traffic simulator written in object-oriented VB.NET")> 
<Assembly: AssemblyCompany("PME Programs; Philip Edwards")> 
<Assembly: AssemblyProduct("TrafficSimulator")> 
<Assembly: AssemblyCopyright("Released under GPL v3.0")> 
<Assembly: AssemblyTrademark("Released under GPL v3.0")> 

<Assembly: ComVisible(False)>

'The following GUID is for the ID of the typelib if this project is exposed to COM
<Assembly: Guid("8eadc212-aee8-49fa-9238-6b7d40dd1d55")> 

' Version information for an assembly consists of the following four values:
'
'      Major Version
'      Minor Version 
'      Build Number
'      Revision
'
' You can specify all the values or you can default the Build and Revision Numbers 
' by using the '*' as shown below:
' <Assembly: AssemblyVersion("1.0.*")> 

<Assembly: AssemblyVersion("0.1.0.0")> 
<Assembly: AssemblyFileVersion("0.1.0.0")> 
