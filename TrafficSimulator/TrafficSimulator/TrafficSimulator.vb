﻿#Region "Imports"
Imports System.Numerics
#End Region
Public Class TrafficSimulator
#Region "Statics"
    Public CarEntryPaths As Integer = 3
    Public TrainEntryPaths As Integer = 2
    Public PedestrianEntryPaths As Integer = 2
    Public CarPrevalence As Integer = 7
    Public VanPrevalence As Integer = 2
    Public TruckPrevalence As Integer = 2
    Public PedestrianPrevalence As Integer = 8
    Public BicyclePrevalence As Integer = 2
    Public TrainPrevalence As Integer = 1
    Public TramPrevalence As Integer = 1
#End Region

#Region "Map details"
    Dim MapRoadPoints As New List(Of Point())
    Dim TrainTracks As New List(Of Point())
    Dim PedestrianPaths As New List(Of Point())
    Dim TrafficLights As New List(Of Point)
    Dim TrafficLightColours As New List(Of Integer)
#End Region

#Region "Declarations"
    Public WithEvents Timer As New Timer
    Public Stopwatch As New Stopwatch
    Public time As ULong
    Public RoadEntryBlocked(CarEntryPaths - 1) As Boolean
    Public TrainEntryBlocked(TrainEntryPaths - 1) As Boolean
    Public SimulationSpeed As Double = 1
    Public MaxID As BigInteger
    Public DebugLabel1, DebugLabel2 As New Label
    Public LastDebugDrawCall As DateTime = Now
#End Region

#Region "Lists"
    Dim Cars As New List(Of Car)
    Dim Vans As New List(Of Van)
    Dim Trucks As New List(Of Truck)
    Dim Bicycles As New List(Of Bicycle)
    Dim Pedestrians As New List(Of Pedestrian)
    Dim Trains As New List(Of Train)
    Dim Trams As New List(Of Tram)
    Dim Vehicles As New List(Of Vehicle)
    Dim RoadVehicles As New List(Of Vehicle)
#End Region

#Region "Form events"
    Private Sub FormLoad(ByVal sender As Object, ByVal e As EventArgs) Handles MyBase.Load
        'Initialise misc
        Me.Font = New Font("Segoe UI Light", 11)

        Me.DoubleBuffered = True

        Me.FormBorderStyle = Windows.Forms.FormBorderStyle.None
        Me.WindowState = FormWindowState.Maximized

        DebugLabel1.Text = "0"
        DebugLabel1.AutoSize = True
        DebugLabel2.AutoSize = True
        DebugLabel2.Top = DebugLabel1.Bottom + 5
        Controls.Add(DebugLabel1)
        Controls.Add(DebugLabel2)

        Stopwatch.Start()

        'Initialise vehicles
        NewRandomVehicles()


        'Initialise map
        MapRoadPoints.Add({New Point(0, 100), New Point(Me.Width, 100)})
        MapRoadPoints.Add({New Point(0, 150), New Point(Me.Width, 150)})
        MapRoadPoints.Add({New Point(500, 150), New Point(500, Me.Height)})
        MapRoadPoints.Add({New Point(550, 150), New Point(550, Me.Height)})
        MapRoadPoints.Add({New Point(500, 580), New Point(550, 580)})
        MapRoadPoints.Add({New Point(500, 690), New Point(550, 690)})

        TrainTracks.Add({New Point(0, 600), New Point(Me.Width, 600)})
        TrainTracks.Add({New Point(0, 620), New Point(Me.Width, 620)})
        TrainTracks.Add({New Point(0, 650), New Point(Me.Width, 650)})
        TrainTracks.Add({New Point(0, 670), New Point(Me.Width, 670)})

        PedestrianPaths.Add({New Point(0, 244), New Point(Me.Width, 244)})
        PedestrianPaths.Add({New Point(0, 256), New Point(Me.Width, 256)})

        TrafficLights.Add(New Point(497, 97))
        TrafficLights.Add(New Point(497, 153))
        TrafficLights.Add(New Point(553, 153))
        TrafficLights.Add(New Point(498, 692))
        TrafficLights.Add(New Point(552, 578))

        TrafficLightColours.Add(2)
        TrafficLightColours.Add(0)
        TrafficLightColours.Add(0)
        TrafficLightColours.Add(2)
        TrafficLightColours.Add(2)


        'Do other stuff
        TextBox1.ScrollBars = ScrollBars.Vertical

        Timer.Interval = 1
        Timer.Start()
    End Sub

    Private Sub FormPaint(ByVal sender As Object, ByVal e As PaintEventArgs) Handles MyBase.Paint
        e.Graphics.SmoothingMode = Drawing2D.SmoothingMode.AntiAlias
        e.Graphics.TextRenderingHint = Drawing.Text.TextRenderingHint.ClearTypeGridFit

        e.Graphics.DrawRectangle(Pens.White, New Rectangle(New Point(0, 0), Me.Size))

        Dim stringFormat As New StringFormat()
        stringFormat.Alignment = StringAlignment.Center
        stringFormat.LineAlignment = StringAlignment.Center
        e.Graphics.DrawString("Press Esc to exit; press Space to pause/resume simulation; press F3/F2 to +/- simulation speed; press Enter to add vehicles", New Font("Segoe UI Light", 19, FontStyle.Bold), Brushes.Black, New Point(Me.Width / 2, 30), stringFormat)
        Dim debugtext As String = ""
        If Now.Subtract(LastDebugDrawCall).TotalMilliseconds > 16 Then
            For i = 0 To Vehicles.Count - 1
                debugtext &= "Type: " & Vehicles(i).ToString.Substring(Me.Name.Length + 1) & ", X: " & Math.Round(Vehicles(i).X) & ", Y: " & Math.Round(Vehicles(i).Y) & ", vX: " & Math.Round(Vehicles(i).vX) & ", vY: " & Math.Round(Vehicles(i).vY) & ", ID: " & Vehicles(i).ID & vbNewLine
            Next
            If Not debugtext = TextBox1.Text Then TextBox1.Text = debugtext : LastDebugDrawCall = Now
        End If

        For i = 0 To MapRoadPoints.Count - 1
            e.Graphics.DrawLines(Pens.Black, MapRoadPoints(i))
        Next

        For i = 0 To TrainTracks.Count - 1
            e.Graphics.DrawLines(Pens.Gray, TrainTracks(i))
        Next

        For i = 0 To PedestrianPaths.Count - 1
            e.Graphics.DrawLines(Pens.DarkBlue, PedestrianPaths(i))
        Next

        For i = 0 To TrafficLights.Count - 1
            Dim TrafficLightRectangle As New Rectangle(New Point(TrafficLights(i).X - 2, TrafficLights(i).Y - 2), New Size(4, 4))
            'Dim points() = {New Point(TrafficLights(i).X - 1, TrafficLights(i).Y - 1), New Point(TrafficLights(i).X + 1, TrafficLights(i).Y - 1), New Point(TrafficLights(i).X - 1, TrafficLights(i).Y + 1), New Point(TrafficLights(i).X + 1, TrafficLights(i).Y + 1), New Point(TrafficLights(i).X, TrafficLights(i).Y)}
            'If TrafficLightColours(i) = "red" Then e.Graphics.DrawPolygon(Pens.Red, points)
            'If TrafficLightColours(i) = "yellow" Then e.Graphics.DrawPolygon(Pens.Yellow, points)
            'If TrafficLightColours(i) = "green" Then e.Graphics.DrawPolygon(Pens.Green, points)
            If TrafficLightColours(i) = 0 Then e.Graphics.FillRectangle(Brushes.Red, TrafficLightRectangle)
            If TrafficLightColours(i) = 1 Then e.Graphics.FillRectangle(Brushes.Yellow, TrafficLightRectangle)
            If TrafficLightColours(i) = 2 Then e.Graphics.FillRectangle(Brushes.Green, TrafficLightRectangle)
        Next

        Dim PenIDs As Pen() = {Pens.Blue, Pens.Gray, Pens.Red, Pens.Brown, Pens.Orange, Pens.Purple, Pens.DeepSkyBlue}
        Dim PenToUse As Pen = Nothing
        For Each Vehicle As Vehicle In Vehicles
            Select Case Vehicle.ToString.Substring(Me.Name.Length + 1)
                Case "Car"
                    PenToUse = PenIDs(0)
                Case "Van"
                    PenToUse = PenIDs(1)
                Case "Truck"
                    PenToUse = PenIDs(2)
                Case "Pedestrian"
                    PenToUse = PenIDs(3)
                Case "Bicycle"
                    PenToUse = PenIDs(4)
                Case "Train"
                    PenToUse = PenIDs(5)
                Case "Tram"
                    PenToUse = PenIDs(6)
            End Select
            Dim VehicleRectangle As Point() = {New Point(Vehicle.X + 10, Vehicle.Y + 10), New Point(Vehicle.X + 10, Vehicle.Y - 10), New Point(Vehicle.X - 10, Vehicle.Y - 10), New Point(Vehicle.X - 10, Vehicle.Y + 10)}
            e.Graphics.DrawPolygon(PenToUse, VehicleRectangle)
        Next

        'For Each Car As Car In Cars
        '    Dim CarRectangle As Point() = {New Point(Car.X + 10, Car.Y + 10), New Point(Car.X + 10, Car.Y - 10), New Point(Car.X - 10, Car.Y - 10), New Point(Car.X - 10, Car.Y + 10)}
        '    e.Graphics.DrawPolygon(Pens.Blue, CarRectangle)
        'Next
        'For Each Van As Van In Vans
        '    Dim VanRectangle As Point() = {New Point(Van.X + 10, Van.Y + 10), New Point(Van.X + 10, Van.Y - 10), New Point(Van.X - 10, Van.Y - 10), New Point(Van.X - 10, Van.Y + 10)}
        '    e.Graphics.DrawPolygon(Pens.Gray, VanRectangle)
        'Next
        'For Each Truck As Truck In Trucks
        '    Dim TruckRectangle As Point() = {New Point(Truck.X + 10, Truck.Y + 10), New Point(Truck.X + 10, Truck.Y - 10), New Point(Truck.X - 10, Truck.Y - 10), New Point(Truck.X - 10, Truck.Y + 10)}
        '    e.Graphics.DrawPolygon(Pens.Red, TruckRectangle)
        'Next
        'For Each Pedestrian As Pedestrian In Pedestrians
        '    Dim PedestrianRectangle As Point() = {New Point(Pedestrian.X + 10, Pedestrian.Y + 10), New Point(Pedestrian.X + 10, Pedestrian.Y - 10), New Point(Pedestrian.X - 10, Pedestrian.Y - 10), New Point(Pedestrian.X - 10, Pedestrian.Y + 10)}
        '    e.Graphics.DrawPolygon(Pens.Brown, PedestrianRectangle)
        'Next
        'For Each Bicycle As Bicycle In Bicycles
        '    Dim BicycleRectangle As Point() = {New Point(Bicycle.X + 10, Bicycle.Y + 10), New Point(Bicycle.X + 10, Bicycle.Y - 10), New Point(Bicycle.X - 10, Bicycle.Y - 10), New Point(Bicycle.X - 10, Bicycle.Y + 10)}
        '    e.Graphics.DrawPolygon(Pens.Orange, BicycleRectangle)
        'Next
        'For Each Train As Train In Trains
        '    Dim TrainRectangle As Point() = {New Point(Train.X + 10, Train.Y + 10), New Point(Train.X + 10, Train.Y - 10), New Point(Train.X - 10, Train.Y - 10), New Point(Train.X - 10, Train.Y + 10)}
        '    e.Graphics.DrawPolygon(Pens.Purple, TrainRectangle)
        'Next
        'For Each Tram As Tram In Trams
        '    Dim TramRectangle As Point() = {New Point(Tram.X + 10, Tram.Y + 10), New Point(Tram.X + 10, Tram.Y - 10), New Point(Tram.X - 10, Tram.Y - 10), New Point(Tram.X - 10, Tram.Y + 10)}
        '    e.Graphics.DrawPolygon(Pens.DeepSkyBlue, TramRectangle)
        'Next
    End Sub

    Private Sub KeyDownPress(ByVal sender As Object, ByVal e As KeyEventArgs) Handles MyBase.KeyDown
        Select Case e.KeyCode
            Case Keys.Escape
                Me.Hide()
                Me.Dispose()
                End
            Case Keys.Space
                Timer.Enabled = Not Timer.Enabled

            Case Keys.F3
                SimulationSpeed *= 1.1
                AdjustVelocities()

            Case Keys.F2
                SimulationSpeed *= 0.9
                AdjustVelocities()

            Case Keys.Enter
                NewRandomVehicles()
        End Select
    End Sub

    Private Sub TimerTick(ByVal sender As Object, ByVal e As EventArgs) Handles Timer.Tick
        Dim ticktime = Now
        time += 1

        AlignLists()

        AI()

        MoveVehicles()

        Dim Count As Integer = Int(DebugLabel1.Text)
        Count += 1

        DebugLabel1.Text = Count


        If time Mod 1000 = 0 Then
            NewRandomVehicles()
            time = 0
        End If

        Refresh()
        If time Mod 50 = 0 Then
            DebugLabel2.Text = Math.Round(Count / Stopwatch.Elapsed.Seconds.ToString) & " timer ticks per second; " & Now.Subtract(ticktime).TotalMilliseconds.ToString & " ms taken"
        End If
    End Sub
#End Region

#Region "Subs"
    Private Sub AI()
        'Change traffic lights
        Dim lowestcolour As Integer = 2

        For Each Train As Train In Trains
            If (Train.X > 300 And Train.X <= 400) And Train.vX > 0 Then
                If lowestcolour > 1 Then lowestcolour = 1
            End If
            If (Train.X < 750 And Train.X >= 650) And Train.vX < 0 Then
                If lowestcolour > 1 Then lowestcolour = 1
            End If
            If (Train.X > 400 And Train.X < 650) Then
                lowestcolour = 0
            End If
        Next
        TrafficLightColours(3) = lowestcolour
        TrafficLightColours(4) = lowestcolour


        'Stop at traffic lights (at some point also slow down)



        'Slow down for slower vehicles
        For Each RoadVehicle As RoadVehicle In RoadVehicles
            Dim VehicleAhead = GetVehicleAhead(RoadVehicle)
            If Not VehicleAhead Is Nothing Then
                If RoadVehicle.vX > 0 Then
                    'Going right
                    If RoadVehicle.vX > VehicleAhead.vX Then
                        RoadVehicle.BrakesApplied = True
                    Else
                        RoadVehicle.BrakesApplied = False
                    End If
                End If
                If RoadVehicle.vX < 0 Then
                    'Going left
                    If RoadVehicle.vX < VehicleAhead.vX Then
                        RoadVehicle.BrakesApplied = True
                    Else
                        RoadVehicle.BrakesApplied = False
                    End If
                End If
                If RoadVehicle.vY < 0 Then
                    'Going up
                    If RoadVehicle.vY < VehicleAhead.vY Then
                        RoadVehicle.BrakesApplied = True
                    Else
                        RoadVehicle.BrakesApplied = False
                    End If
                End If
            End If
        Next

        For Each Train As Train In Trains
            Dim VehicleAhead As Object = GetVehicleAhead(Train)
            If Not VehicleAhead Is Nothing Then
                If Train.vX > 0 Then
                    'Going right

                End If
                If Train.vX < 0 Then
                    'Going left

                End If
            End If
        Next


        'Dim debuglabel As New Label
        'debuglabel.AutoSize = True
        'Dim blah As New Vehicle()
        'debuglabel.Text = blah.Type.ToString
        'Controls.Add(debuglabel)





        'Overtake if safe



    End Sub

    Private Sub AdjustVelocities()
        For Each Vehicle As Vehicle In Vehicles
            Vehicle.vX *= SimulationSpeed
            Vehicle.vY *= SimulationSpeed
        Next
        AlignLists()
    End Sub

    Private Sub AlignLists()
        For Each Vehicle As Vehicle In Vehicles
            For Each Car As Car In Cars
                If Car.ID = Vehicle.ID Then Car = Vehicle
            Next
            For Each Van As Van In Vans
                If Van.ID = Vehicle.ID Then Van = Vehicle
            Next
            For Each Truck As Truck In Trucks
                If Truck.ID = Vehicle.ID Then Truck = Vehicle
            Next
            For Each Pedestrian As Pedestrian In Pedestrians
                If Pedestrian.ID = Vehicle.ID Then Pedestrian = Vehicle
            Next
            For Each Tram As Tram In Trams
                If Tram.ID = Vehicle.ID Then Tram = Vehicle
            Next
            For Each Train As Train In Trains
                If Train.ID = Vehicle.ID Then Train = Vehicle
            Next
            For Each Bicycle As Bicycle In Bicycles
                If Bicycle.ID = Vehicle.ID Then Bicycle = Vehicle
            Next
            For Each RoadVehicle As Vehicle In RoadVehicles
                If RoadVehicle.ID = Vehicle.ID Then RoadVehicle = Vehicle
            Next
        Next
    End Sub

    Private Sub MoveVehicles()
        AlignLists()

        'Increase braking (to a point) if brakes are applied
        For Each Vehicle As Vehicle In Vehicles
            If Not TypeOf Vehicle Is Pedestrian Then
                'Braking
                If Vehicle.BrakesApplied = True Then
                    If Vehicle.vX < 0 Then
                        'Going right
                        If Not Vehicle.dvX <= -0.1 Then Vehicle.dvX -= 0.02
                    End If
                    If Vehicle.vX > 0 Then
                        'Going left
                        If Not Vehicle.dvX >= +0.1 Then Vehicle.dvX += 0.02
                    End If
                    If Vehicle.vY < 0 Then
                        'Going down
                        If Not Vehicle.dvY <= -0.1 Then Vehicle.dvY -= 0.02
                    End If
                    If Vehicle.vY > 0 Then
                        'Going up
                        If Not Vehicle.dvY >= +0.1 Then Vehicle.dvY += 0.02
                    End If
                Else
                    'Stop decelerating if brakes are released
                    If Not Vehicle.dvX = 0 And Not Vehicle.dvY = 0 And Not Vehicle.AcceleratorApplied Then

                    End If
                End If
                If Vehicle.AcceleratorApplied = True Then
                    If Vehicle.vX < 0 Then
                        'Going right
                        If Not Vehicle.dvX >= +0.1 Then Vehicle.dvX += 0.02
                    End If
                    If Vehicle.vX > 0 Then
                        'Going left
                        If Not Vehicle.dvX <= -0.1 Then Vehicle.dvX -= 0.02
                    End If
                    If Vehicle.vY < 0 Then
                        'Going down
                        If Not Vehicle.dvY >= +0.1 Then Vehicle.dvY += 0.02
                    End If
                    If Vehicle.vY > 0 Then
                        'Going up
                        If Not Vehicle.dvY <= -0.1 Then Vehicle.dvY -= 0.02
                    End If
                Else

                End If
            End If
        Next

        'Move vehicles, deleting vehicles that are already offscreen and going further off
        'Dim NewArray As New List(Of Vehicle)
        For i = Vehicles.Count - 1 To 0 Step -1
            If ((Vehicles(i).X <= -10 And Vehicles(i).vX < 0) Or (Vehicles(i).X >= Me.Width + 10 And Vehicles(i).vX > 0) Or (Vehicles(i).Y <= -10 And Vehicles(i).vY < 0) Or (Vehicles(i).Y >= Me.Height + 10 And Vehicles(i).vY > 0)) Then
                Vehicles.RemoveAt(i)
                'Dim tempme = i + 1
                'i = tempme - 1
                'NewArray.Add(Vehicle)
            Else
                Vehicles(i).X += Vehicles(i).vX
                Vehicles(i).Y += Vehicles(i).vY
                Vehicles(i).vX += Vehicles(i).dvX
                Vehicles(i).vY += Vehicles(i).dvY
                'If Vehicle.Type = "Car" Or Vehicle.Type = "Van" Or Vehicle.Type = "Truck" Or Vehicle.Type = "Bicycle" Or Vehicle.Type = "Tram" Then
                '    If RoadEntryBlocked(0) = True Then If Vehicle.X >= 20 Then RoadEntryBlocked(0) = False
                '    If RoadEntryBlocked(1) = True then If Vehicle.x <= 
                'End If
            End If
        Next
        'Array.Copy(NewArray.ToArray, Vehicles.ToArray, NewArray.Count)

        'Get rid of entry blocking if entry is free
        For i = 0 To RoadEntryBlocked.Count - 1
            If RoadEntryBlocked(i) = True Then
                Dim FurthestVehicle As New Vehicle
                If Not RoadVehicles.Count = 0 Then
                    FurthestVehicle = RoadVehicles(0)
                End If

                For Each Vehicle As Vehicle In RoadVehicles
                    If i = 0 Then If Vehicle.X < FurthestVehicle.X Then FurthestVehicle = Vehicle
                    If i = 1 Then If Vehicle.X > FurthestVehicle.X Then FurthestVehicle = Vehicle
                    If i = 2 Then If Vehicle.Y > FurthestVehicle.Y Then FurthestVehicle = Vehicle
                Next
                If i = 0 Then If FurthestVehicle.X >= 20 Then RoadEntryBlocked(0) = False
                If i = 1 Then If FurthestVehicle.X <= Me.Width - 20 Then RoadEntryBlocked(1) = False
                If i = 2 Then If FurthestVehicle.Y <= Me.Height - 20 Then RoadEntryBlocked(2) = False
            End If
        Next

        For i = 0 To TrainEntryBlocked.Count - 1
            If TrainEntryBlocked(i) = True Then
                Dim FurthestTrain As New Train
                If Not Trains.Count = 0 Then
                    FurthestTrain = Trains(0)
                End If

                For Each Train As Train In Trains
                    If i = 0 Then If Train.X < FurthestTrain.X Then FurthestTrain = Train
                    If i = 1 Then If Train.X > FurthestTrain.X Then FurthestTrain = Train
                Next
                If i = 0 Then If FurthestTrain.X >= 20 Then TrainEntryBlocked(0) = False
                If i = 1 Then If FurthestTrain.X <= Me.Width - 20 Then TrainEntryBlocked(1) = False
            End If
        Next

        AlignLists()
    End Sub

    Private Sub NewRandomVehicles()
        Dim RandomGenerator As New RandomNumber

        For i = 0 To RandomGenerator.NewRandom(0, CarPrevalence)
            Dim Car1 As New Car
            Dim EntryLocation As New RandomEntry(Car1)
            If Not Blank(EntryLocation) Then
                Car1.X = EntryLocation.X
                Car1.Y = EntryLocation.Y
                Car1.vX = EntryLocation.vX
                Car1.vY = EntryLocation.vY
                Car1.ID = MaxID + 1
                Cars.Add(Car1)
                Vehicles.Add(Car1)
                RoadVehicles.Add(Car1)
            End If
        Next
        For i = 0 To RandomGenerator.NewRandom(0, VanPrevalence)
            Dim Van1 As New Van
            Dim EntryLocation As New RandomEntry(Van1)
            If Not Blank(EntryLocation) Then
                Van1.X = EntryLocation.X
                Van1.Y = EntryLocation.Y
                Van1.vX = EntryLocation.vX
                Van1.vY = EntryLocation.vY
                Van1.ID = MaxID + 1
                Vans.Add(Van1)
                Vehicles.Add(Van1)
                RoadVehicles.Add(Van1)
            End If
        Next
        For i = 0 To RandomGenerator.NewRandom(0, PedestrianPrevalence)
            Dim Pedestrian1 As New Pedestrian
            Dim EntryLocation As New RandomEntry(Pedestrian1)
            If Not Blank(EntryLocation) Then
                Pedestrian1.X = EntryLocation.X
                Pedestrian1.Y = EntryLocation.Y
                Pedestrian1.vX = EntryLocation.vX
                Pedestrian1.vY = EntryLocation.vY
                Pedestrian1.ID = MaxID + 1
                Pedestrians.Add(Pedestrian1)
                Vehicles.Add(Pedestrian1)
            End If
        Next
        For i = 0 To RandomGenerator.NewRandom(0, TruckPrevalence)
            Dim Truck1 As New Truck
            Dim EntryLocation As New RandomEntry(Truck1)
            If Not Blank(EntryLocation) Then
                Truck1.X = EntryLocation.X
                Truck1.Y = EntryLocation.Y
                Truck1.vX = EntryLocation.vX
                Truck1.vY = EntryLocation.vY
                Truck1.ID = MaxID + 1
                Trucks.Add(Truck1)
                Vehicles.Add(Truck1)
                RoadVehicles.Add(Truck1)
            End If
        Next
        For i = 0 To RandomGenerator.NewRandom(0, BicyclePrevalence)
            Dim Bicycle1 As New Bicycle
            Dim EntryLocation As New RandomEntry(Bicycle1)
            If Not Blank(EntryLocation) Then
                Bicycle1.X = EntryLocation.X
                Bicycle1.Y = EntryLocation.Y
                Bicycle1.vX = EntryLocation.vX
                Bicycle1.vY = EntryLocation.vY
                Bicycle1.ID = MaxID + 1
                Bicycles.Add(Bicycle1)
                Vehicles.Add(Bicycle1)
                RoadVehicles.Add(Bicycle1)
            End If
        Next
        For i = 0 To RandomGenerator.NewRandom(0, TrainPrevalence)
            Dim Train1 As New Train
            Dim EntryLocation As New RandomEntry(Train1)
            If Not Blank(EntryLocation) Then
                Train1.X = EntryLocation.X
                Train1.Y = EntryLocation.Y
                Train1.vX = EntryLocation.vX
                Train1.vY = EntryLocation.vY
                Train1.ID = MaxID + 1
                Trains.Add(Train1)
                Vehicles.Add(Train1)
            End If
        Next
        For i = 0 To RandomGenerator.NewRandom(0, TramPrevalence)
            Dim Tram1 As New Tram
            Dim EntryLocation As New RandomEntry(Tram1)
            If Not Blank(EntryLocation) Then
                Tram1.X = EntryLocation.X
                Tram1.Y = EntryLocation.Y
                Tram1.vX = EntryLocation.vX
                Tram1.vY = EntryLocation.vY
                Tram1.ID = MaxID + 1
                Trams.Add(Tram1)
                Vehicles.Add(Tram1)
                RoadVehicles.Add(Tram1)
            End If
        Next
    End Sub
#End Region

#Region "Functions"
    Private Function Blank(ByVal Entry As RandomEntry)
        Dim IsBlank As Boolean = True
        If Not Entry.X = 0 Then IsBlank = False
        If Not Entry.Y = 0 Then IsBlank = False
        If Not Entry.vX = 0 Then IsBlank = False
        If Not Entry.vY = 0 Then IsBlank = False
        Return IsBlank
    End Function

    Private Function GetVehicleAhead(ByVal VehicleInQuestion As Vehicle) As Vehicle
        Dim VehicleAhead As New Vehicle()

        If TypeOf VehicleInQuestion Is RoadVehicle Then
            If VehicleInQuestion.vX > 0 Then
                'Going to right
                Dim GoingRight As New List(Of RoadVehicle)
                For Each RoadVehicle As RoadVehicle In RoadVehicles
                    If RoadVehicle.vX > 0 And RoadVehicle.X > VehicleInQuestion.X Then GoingRight.Add(RoadVehicle)
                Next

                If Not GoingRight.Count = 0 Then
                    Dim FurthestLeft As RoadVehicle = GoingRight(0)
                    For Each RoadVehicle As RoadVehicle In GoingRight
                        If RoadVehicle.X < FurthestLeft.X Then FurthestLeft = RoadVehicle
                    Next
                    VehicleAhead = FurthestLeft
                End If

            ElseIf VehicleInQuestion.vX < 0 Then
                'Going to left
                Dim GoingLeft As New List(Of RoadVehicle)
                For Each RoadVehicle As RoadVehicle In RoadVehicles
                    If RoadVehicle.vX < 0 And RoadVehicle.X < VehicleInQuestion.X Then GoingLeft.Add(RoadVehicle)
                Next

                If Not GoingLeft.Count = 0 Then
                    Dim FurthestRight As RoadVehicle = GoingLeft(0)
                    For Each RoadVehicle As RoadVehicle In GoingLeft
                        If RoadVehicle.X > FurthestRight.X Then FurthestRight = RoadVehicle
                    Next
                    VehicleAhead = FurthestRight
                End If

            End If
            If VehicleInQuestion.vY > 0 Then
                'Going up
                Dim GoingUp As New List(Of RoadVehicle)
                For Each RoadVehicle As RoadVehicle In RoadVehicles
                    If RoadVehicle.vY < 0 And RoadVehicle.Y < VehicleInQuestion.Y Then GoingUp.Add(RoadVehicle)
                Next

                If Not GoingUp.Count = 0 Then
                    Dim FurthestDown As RoadVehicle = GoingUp(0)
                    For Each RoadVehicle As RoadVehicle In GoingUp
                        If RoadVehicle.Y > FurthestDown.Y Then FurthestDown = RoadVehicle
                    Next
                    VehicleAhead = FurthestDown
                End If
            End If
        End If

        If TypeOf VehicleInQuestion Is Train Then


























        End If

        If TypeOf VehicleInQuestion Is Pedestrian Then

        End If

        Return VehicleAhead
    End Function
#End Region
End Class

#Region "Random classes"
Public Class RandomNumber
    Public Function NewRandom(min As Integer, max As Integer) As Integer
        ' by making Generator static, we preserve the same instance '
        ' (i.e., do not create new instances with the same seed over and over) '
        ' between calls '
        Randomize()
        Static Generator As System.Random = New System.Random()
        Return Generator.Next(min, max)
    End Function
End Class

Public Class RandomEntry
    Public X, Y, vX, vY As Double
    Public ID As Integer
    Public BrakesApplied As Boolean = False

    Public Sub New(vehicle As Vehicle)
        If vehicle.Type = "Car" Or vehicle.Type = "Van" Or vehicle.Type = "Truck" Or vehicle.Type = "Bicycle" Or vehicle.Type = "Tram" Then
            Dim Randomness As New RandomNumber
            Dim RandEntryLoc As Integer = Randomness.NewRandom(0, TrafficSimulator.CarEntryPaths)
            Select Case RandEntryLoc
                Case 0
                    If Not TrafficSimulator.RoadEntryBlocked(0) = True Then
                        TrafficSimulator.RoadEntryBlocked(0) = True
                        X = 0
                        Y = 112
                        vX = (Randomness.NewRandom(vehicle.MaxSpeed / 10, vehicle.MaxSpeed)) / (100 * TrafficSimulator.SimulationSpeed)
                        vY = 0
                    End If
                Case 1
                    If Not TrafficSimulator.RoadEntryBlocked(1) = True Then
                        TrafficSimulator.RoadEntryBlocked(1) = True
                        X = 512
                        Y = TrafficSimulator.Height
                        vX = 0
                        vY = (-Randomness.NewRandom(vehicle.MaxSpeed / 10, vehicle.MaxSpeed)) / (100 * TrafficSimulator.SimulationSpeed)
                    End If
                Case 2
                    If Not TrafficSimulator.RoadEntryBlocked(2) = True Then
                        TrafficSimulator.RoadEntryBlocked(2) = True
                        X = TrafficSimulator.Width
                        Y = 138
                        vX = (-Randomness.NewRandom(vehicle.MaxSpeed / 10, vehicle.MaxSpeed)) / (100 * TrafficSimulator.SimulationSpeed)
                        vY = 0
                    End If
            End Select

        ElseIf vehicle.Type = "Train" Then
            Dim Randomness As New RandomNumber
            Dim RandEntryLoc As Integer = Randomness.NewRandom(0, TrafficSimulator.TrainEntryPaths)
            Select Case RandEntryLoc
                Case 0
                    If Not TrafficSimulator.TrainEntryBlocked(0) = True Then
                        TrafficSimulator.TrainEntryBlocked(0) = True
                        X = 0
                        Y = 610
                        vX = (Randomness.NewRandom(vehicle.MaxSpeed / 10, vehicle.MaxSpeed)) / (100 * TrafficSimulator.SimulationSpeed)
                        vY = 0
                    End If
                Case 1
                    If Not TrafficSimulator.TrainEntryBlocked(1) = True Then
                        TrafficSimulator.TrainEntryBlocked(1) = True
                        X = TrafficSimulator.Width
                        Y = 660
                        vX = (-Randomness.NewRandom(vehicle.MaxSpeed / 10, vehicle.MaxSpeed)) / (100 * TrafficSimulator.SimulationSpeed)
                        vY = 0
                    End If
            End Select

        ElseIf vehicle.Type = "Pedestrian" Then
            Dim Randomness As New RandomNumber
            Dim RandEntryLoc As Integer = Randomness.NewRandom(0, TrafficSimulator.PedestrianEntryPaths)
            Select Case RandEntryLoc
                Case 0
                    X = 0
                    Y = 250
                    vX = (Randomness.NewRandom(vehicle.MaxSpeed / 5, vehicle.MaxSpeed)) / (100 * TrafficSimulator.SimulationSpeed)
                    vY = 0
                Case 1
                    X = TrafficSimulator.Width
                    Y = 250
                    vX = (-Randomness.NewRandom(vehicle.MaxSpeed / 5, vehicle.MaxSpeed)) / (100 * TrafficSimulator.SimulationSpeed)
                    vY = 0
            End Select
        End If

        vehicle.ID = TrafficSimulator.MaxID + 1
        TrafficSimulator.MaxID = vehicle.ID
    End Sub
End Class
#End Region

#Region "Vehicle classes"
Public Class Car : Inherits RoadVehicle
    Public Sub New()
        Me.MaxSpeed = 130
        Me.Type = "Car"
    End Sub
End Class

Public Class Van : Inherits RoadVehicle
    Public Sub New()
        Me.MaxSpeed = 115
        Me.Type = "Van"
    End Sub
End Class

Public Class Truck : Inherits RoadVehicle
    Public Sub New()
        Me.MaxSpeed = 100
        Me.Type = "Truck"
    End Sub
End Class

Public Class Bicycle : Inherits RoadVehicle
    Public Sub New()
        Me.MaxSpeed = 20
        Me.Type = "Bicycle"
    End Sub
End Class

Public Class Pedestrian : Inherits Vehicle
    Public Sub New()
        Me.MaxSpeed = 5
        Me.Type = "Pedestrian"
    End Sub
End Class

Public Class Train : Inherits Vehicle
    Public Sub New()
        Me.MaxSpeed = 200
        Me.Type = "Train"
    End Sub
End Class

Public Class Tram : Inherits RoadVehicle
    Public Sub New()
        Me.MaxSpeed = 80
        Me.Type = "Tram"
    End Sub
End Class
#End Region

#Region "Misc"
Public Class Vehicle
    Public X, Y, vX, vY, dvX, dvY As Double
    Public MaxSpeed, ID As Integer
    Public BrakesApplied As Boolean = False
    Public AcceleratorApplied As Boolean = False
    Public Type As String = "None"
End Class

Public Class RoadVehicle : Inherits Vehicle
End Class
#End Region

#Region "Old code"
'Private Function trafficsimulator.maxid As Integer
'    Dim HighestID As Integer = 0
'    For Each Vehicle As Vehicle In Vehicles
'        If Vehicle.ID > HighestID Then HighestID = Vehicle.ID
'    Next
'    Return HighestID
'End Function

'Dim shouldremove(Vehicles.Count - 1) As Boolean
'For i = 0 To Vehicles.Count - 1
'    If (Vehicles(i).X >= 0 And Vehicles(i).vX < 0) Or (Vehicles(i).X <= Me.Width And Vehicles(i).vX > 0) Or (Vehicles(i).Y >= 0 And Vehicles(i).vY < 0) Or (Vehicles(i).Y <= Me.Height And Vehicles(i).vY > 0) Then
'        Vehicles.RemoveAt(i)
'        Vehicles(i).X += Vehicles(i).vX
'        Vehicles(i).Y += Vehicles(i).vY
'        Vehicles(i).vX += Vehicles(i).dvX
'        Vehicles(i).vY += Vehicles(i).dvY
'    Else
'        Vehicles(i).X += Vehicles(i).vX
'        Vehicles(i).Y += Vehicles(i).vY
'        Vehicles(i).vX += Vehicles(i).dvX
'        Vehicles(i).vY += Vehicles(i).dvY
'    End If
'Next
'For Each Vehicle As Vehicle In Vehicles
'    i += 1
'    If (Vehicle.X >= 0 And Vehicle.vX < 0) Or (Vehicle.X <= Me.Width And Vehicle.vX > 0) Or (Vehicle.Y >= 0 And Vehicle.vY < 0) Or (Vehicle.Y <= Me.Height And Vehicle.vY > 0) Then
'        shouldremove(i) = True
'    Else
'        Vehicle.X += Vehicle.vX
'        Vehicle.Y += Vehicle.vY
'        Vehicle.vX += Vehicle.dvX
'        Vehicle.vY += Vehicle.dvY
'    End If
'Next
'For i = 0 To shouldremove.Count - 1
'    If shouldremove(i) Then Vehicles.RemoveAt(i)
'Next
#End Region





'----------------------------------------------------------------TODOS----------------------------------------------------------------

#Region "Todos"
'- ACCELERATION/DECELERATION - FIX IT
'- FINISH SLOWING DOWN AI LOGIC
'- STOP AT TRAFFIC LIGHTS
'- OVERTAKE IF SAFE
'- MAKE RANDOM ENTRIES MORE RANDOM BY RANDOMLY CHOOSING TYPE, NOT JUST NUMBER, OF VEHICLES
#End Region